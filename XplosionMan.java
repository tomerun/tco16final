import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;

public class XplosionMan {

    private final long startTime = System.currentTimeMillis();
    private static long TL = 10000;
    private static boolean DEBUG = true;
    private static final int[] DR = {-1, 1, 0, 0};
    private static final int[] DC = {0, 0, -1, 1};
    private static final char[] DIR_CHARS = "UDLRUDLR".toCharArray();
    private static final char HARD = '#';
    private static final char SOFT = '+';
    private static final char EMPTY = '.';
    private static final char BOMB = '*';
    int N, T, B, R, TC, RC;
    char[][] fieldO;
    int[][] ticks;
    int[][] backDir;
    CountBuf bfsBuf;
    ArrayList<Character> cmdBuf = new ArrayList<>();

    String makeMoves(String[] board, int T, int B, int R) {
        N = board.length;
        bfsBuf = new CountBuf(N);
        this.T = T;
        this.B = B;
        this.R = R;
        fieldO = new char[N][N];
        ticks = new int[N][N];
        backDir = new int[N][N];
        for (int i = 0; i < N; i++) {
            fieldO[i] = board[i].toCharArray();
        }
        Solution sol = solve();
        debug(sol.commands);
        return sol.commands;
    }

    Solution solve() {
        Simulation sim = new Simulation();
        while (nextMove(sim)) {
        }
        Solution sol = new Solution();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sim.cmds.size(); i++) {
            sb.append(sim.cmds.get(i));
        }
        sol.commands = sb.toString();
        sol.score = sim.score;
        return sol;
    }

    boolean nextMove(Simulation sim) {
        if (findPowerup(sim)) {
            return true;
        }
        if (explodeToPowerup(sim)) {
            return true;
        }
        if (destroy(sim)) {
            return true;
        }
        return false;
    }

    boolean findPowerup(Simulation sim) {
        bfsBuf.clear();
        ArrayList<Pos> ps = new ArrayList<>();
        ps.add(new Pos(sim.cr, sim.cc));
        for (int i = 0; i < ps.size(); i++) {
            Pos p = ps.get(i);
            for (int j = 0; j < 4; j++) {
                int nr = p.r + DR[j];
                int nc = p.c + DC[j];
                if (bfsBuf.get(nr, nc)) continue;
                bfsBuf.set(nr, nc);
                backDir[nr][nc] = j;
                char cs = sim.field[nr][nc];
                if (cs == 'T' || cs == 'B' || cs == 'R') {
                    if (cs == 'T') {
                        TC++;
                        T += TC;
                    } else if (cs == 'B') {
                        B++;
                    } else {
                        RC++;
                        R += RC;
                    }
                    cmdBuf.clear();
                    sim.field[nr][nc] = EMPTY;
                    sim.cr = nr;
                    sim.cc = nc;
                    System.err.println(nr + " " + nc);
                    while (nr != ps.get(0).r || nc != ps.get(0).c) {
                        int d = backDir[nr][nc];
                        cmdBuf.add(DIR_CHARS[d]);
                        nr -= DR[d];
                        nc -= DC[d];
                    }
                    for (int k = cmdBuf.size() - 1; k >= 0; k--) {
                        sim.cmds.add(cmdBuf.get(k));
                    }
                    debug("findPowerup:" + sim.cmds);
                    return true;
                } else if (cs == EMPTY) {
                    ps.add(new Pos(nr, nc));
                }
            }
        }
        for (int i = 0; i < N; i++) {
            debug(String.valueOf(sim.field[i]));
        }
        return false;
    }

    boolean explodeToPowerup(Simulation sim) {
        bfsBuf.clear();
        ArrayDeque<Pos> q = new ArrayDeque<>();
        q.add(new Pos(sim.cr, sim.cc));
        while (!q.isEmpty()) {
            Pos p = q.poll();
            for (int j = 0; j < 4; j++) {
                int nr = p.r + DR[j];
                int nc = p.c + DC[j];
                if (bfsBuf.get(nr, nc)) continue;
                bfsBuf.set(nr, nc);
                backDir[nr][nc] = j;
                char cs = sim.field[nr][nc];
                if (cs == 'T' || cs == 'B' || cs == 'R') {
                    cmdBuf.clear();
                    int exr = 0;
                    int exc = 0;
                    while (nr != sim.cr || nc != sim.cc) {
                        int d = backDir[nr][nc];
                        cmdBuf.add(DIR_CHARS[d]);
                        if (sim.field[nr][nc] == SOFT) {
                            cmdBuf.clear();
                            exr = nr - DR[d];
                            exc = nc - DC[d];
                            debug(exr + " " + exc);
                        }
                        nr -= DR[d];
                        nc -= DC[d];
                    }
                    for (int k = cmdBuf.size() - 1; k >= 0; k--) {
                        sim.cmds.add(cmdBuf.get(k));
                    }
                    sim.cr = exr;
                    sim.cc = exc;
                    sim.cmds.add('B');
                    if (T > 1) sim.cmds.add('T');
                    sim.explodeSingle(sim.cr, sim.cc, R);
                    debug("explodeToPowerup:" + sim.cmds);
                    return true;
                } else if (cs == EMPTY) {
                    q.addFirst(new Pos(nr, nc));
                } else if (cs == SOFT) {
                    q.addLast(new Pos(nr, nc));
                }
            }
        }
        return false;
    }

    boolean destroy(Simulation sim) {
        bfsBuf.clear();
        ArrayList<Pos> ps = new ArrayList<>();
        ps.add(new Pos(sim.cr, sim.cc));
        for (int i = 0; i < ps.size(); i++) {
            Pos p = ps.get(i);
            for (int j = 0; j < 4; j++) {
                int nr = p.r + DR[j];
                int nc = p.c + DC[j];
                if (bfsBuf.get(nr, nc)) continue;
                bfsBuf.set(nr, nc);
                backDir[nr][nc] = j;
                char cs = sim.field[nr][nc];
                if (cs == SOFT) {
                    cmdBuf.clear();
                    System.err.println(nr + " " + nc);
                    nr -= DR[j];
                    nc -= DC[j];
                    sim.cr = nr;
                    sim.cc = nc;
                    while (nr != ps.get(0).r || nc != ps.get(0).c) {
                        int d = backDir[nr][nc];
                        cmdBuf.add(DIR_CHARS[d]);
                        nr -= DR[d];
                        nc -= DC[d];
                    }
                    for (int k = cmdBuf.size() - 1; k >= 0; k--) {
                        sim.cmds.add(cmdBuf.get(k));
                    }
                    sim.cmds.add('B');
                    if (T > 1) sim.cmds.add('T');
                    sim.explodeSingle(sim.cr, sim.cc, R);
                    debug("destroy:" + sim.cmds);
                    return true;
                } else if (cs == EMPTY) {
                    ps.add(new Pos(nr, nc));
                }
            }
        }
        return false;
    }


    class Simulation {
        int cr, cc;
        char[][] field;
        int score;
        ArrayList<Character> cmds = new ArrayList<>();

        Simulation() {
            field = new char[N][];
            for (int i = 0; i < N; i++) {
                field[i] = fieldO[i].clone();
            }
            cr = 1;
            cc = 1;
        }

        void explodeSingle(int r, int c, int d) {
            int count = 0;
            for (int i = 0; i < 4; i++) {
                for (int j = 1; j <= d; j++) {
                    int cr = r + DR[i] * j;
                    int cc = c + DC[i] * j;
                    if (field[cr][cc] == SOFT) {
                        field[cr][cc] = EMPTY;
                        count++;
                        break;
                    } else if (field[cr][cc] == HARD) {
                        break;
                    } else if (field[cr][cc] == 'T' || field[cr][cc] == 'B' || field[cr][cc] == 'R') {
                        field[cr][cc] = EMPTY;
                    }
                }
            }
            score += count * count;
        }

        void displayField() {
            for (int i = 0; i < N; i++) {
                debug(String.valueOf(field[i]));
            }
        }
    }

    static class Solution {
        String commands;
        int score;
    }

    static class Pos {
        int r, c;

        Pos(int r, int c) {
            this.r = r;
            this.c = c;
        }
    }

    static void debug(String str) {
        if (DEBUG) System.err.println(str);
    }

    static void debug(Object... obj) {
        if (DEBUG) System.err.println(Arrays.deepToString(obj));
    }

    static void debugf(String fmt, Object... obj) {
        if (DEBUG) System.err.printf(fmt, obj);
    }

    static void debugfln(String fmt, Object... obj) {
        if (DEBUG) System.err.printf(fmt + "\n", obj);
    }

    static class CountBuf {
        int[][] count;
        int turn;

        CountBuf(int size) {
            count = new int[size][size];
        }

        void clear() {
            turn++;
        }

        boolean get(int r, int c) {
            return count[r][c] == turn;
        }

        void set(int r, int c) {
            count[r][c] = turn;
        }
    }
}
