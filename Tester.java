import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.SecureRandom;

class TestCase {
    final Object worldLock = new Object();

    public static final int MIN_BOARD_SIZE = 20;
    public static final int MAX_BOARD_SIZE = 60;
    public static final int MIN_START_R = 1;
    public static final int MAX_START_R = 5;
    public static final int MIN_START_T = 1;
    public static final int MAX_START_T = 5;
    public static final int MIN_START_B = 1;
    public static final int MAX_START_B = 5;
    public static final int MIN_POWERUPS = 5;
    public static final int MAX_POWERUPS = 50;
    public static final double MIN_FILL_RATIO = 0.6;
    public static final double MAX_FILL_RATIO = 0.9;
    public static final int[] dx = {1, -1, 0, 0};
    public static final int[] dy = {0, 0, 1, -1};

    public int boardSize;
    public double fillRatio;
    public char[][] board;
    public int[][] bombTick;
    public int[][] bombR;
    public int T, B, Bmax, R; // Time to xplode, Available bombs, Max bombs, Xplode radius
    public int Tc, Bc, Rc;    // Number of collected power-ups of each type
    public int playX, playY;  // player position
    public int numMoves;      // Number of moves made
    public int numWalls;      // Number of walls remaining
    public long score;         // Total score
    public long scoreLast;     // Score of the latest move

    public TestCase(long seed) throws Exception {
        SecureRandom rnd = SecureRandom.getInstance("SHA1PRNG");
        rnd.setSeed(seed);
        boardSize = rnd.nextInt(MAX_BOARD_SIZE - MIN_BOARD_SIZE + 1) + MIN_BOARD_SIZE;
        if (seed == 1) boardSize = 21;
        boardSize |= 1;// make sure the size is odd
        board = new char[boardSize][boardSize];
        bombTick = new int[boardSize][boardSize];
        bombR = new int[boardSize][boardSize];
        int numPower = rnd.nextInt(MAX_POWERUPS - MIN_POWERUPS + 1) + MIN_POWERUPS;
        fillRatio = rnd.nextDouble() * (MAX_FILL_RATIO - MIN_FILL_RATIO) + MIN_FILL_RATIO;
        R = rnd.nextInt(MAX_START_R - MIN_START_R + 1) + MIN_START_R;
        T = rnd.nextInt(MAX_START_T - MIN_START_T + 1) + MIN_START_T;
        B = rnd.nextInt(MAX_START_B - MIN_START_B + 1) + MIN_START_B;
        Bmax = B;
        // fill the board
        for (int y = 0; y < boardSize; y++) {
            for (int x = 0; x < boardSize; x++) {
                if (x == 0 || y == 0 || x == boardSize - 1 || y == boardSize - 1 || (((x & 1) | (y & 1)) == 0 && rnd.nextDouble() < 0.8)) {
                    board[y][x] = '#'; // solid wall
                } else if (rnd.nextDouble() < fillRatio) {
                    board[y][x] = '.'; // open space
                } else {
                    board[y][x] = '+'; // destructable wall
                }
            }
        }
        for (int i = 0; i < numPower; i++) {
            int x, y;
            do {
                x = 1 + rnd.nextInt(boardSize - 2);
                y = 1 + rnd.nextInt(boardSize - 2);
            } while (board[y][x] == '#');
            int type = rnd.nextInt(3);
            if (type == 0) board[y][x] = 'R';
            if (type == 1) board[y][x] = 'B';
            if (type == 2) board[y][x] = 'T';
        }
        board[1][1] = '.'; // player starts here
        board[2][1] = '.';
        board[1][2] = '.';

        playX = playY = 1;
        numMoves = numWalls = 0;
        for (int y = 0; y < boardSize; y++)
            for (int x = 0; x < boardSize; x++)
                if (board[y][x] == '+') numWalls++;
    }

    public long doXplode(int y, int x) {
        board[y][x] = '?';
        B++; // make bomb available again
        long sum = 0;
        for (int d = 0; d < 4; d++) {
            int px = x + dx[d];
            int py = y + dy[d];
            int dist = bombR[y][x];
            while (dist > 0 && board[py][px] != '#' && board[py][px] != '@') {
                if (board[py][px] == '+') {
                    sum++;
                    numWalls--;
                    board[py][px] = '@';     // Mark destructable wall as counted
                    break;
                } else if (board[py][px] == '*') {
                    sum += doXplode(py, px); // Recursively explode bombs
                }
                board[py][px] = '?';         // Destroy any power-ups in the way
                py += dy[d];
                px += dx[d];
                dist--;
            }
        }
        return sum;
    }

    public void doMove(char mv) {
        synchronized (worldLock) {
            scoreLast = 0;
            // remove flames and burnt walls
            for (int y = 0; y < boardSize; y++)
                for (int x = 0; x < boardSize; x++)
                    if (board[y][x] == '?' || board[y][x] == '@')
                        board[y][x] = '.';
            // tick bombs
            for (int y = 0; y < boardSize; y++)
                for (int x = 0; x < boardSize; x++)
                    if (board[y][x] == '*') {
                        bombTick[y][x]--;
                        if (bombTick[y][x] <= 0) {
                            long sum = doXplode(y, x);
                            scoreLast += sum * sum;
                            score += sum * sum;
                        }
                    }
            // make move
            int newX = playX;
            int newY = playY;
            if (mv == 'R') newX++;
            else if (mv == 'L') newX--;
            else if (mv == 'U') newY--;
            else if (mv == 'D') newY++;
            else if (mv == 'B') {
                // plant a bomb
                if (B == 0) {
                    throw new RuntimeException("INFO: Can not plant a bomb, zero bombs available.");
                } else if (board[playY][playX] == '*') {
                    throw new RuntimeException("INFO: Can not plant a bomb, bomb already planted.");
                } else {
                    board[playY][playX] = '*';
                    bombTick[playY][playX] = T;
                    bombR[playY][playX] = R;
                    B--;
                }
            } else if (mv == 'T') {
                // trigger bomb with lowest count
                int bx = -1, by = 0, bi = 1 << 30;
                for (int y = 0; y < boardSize; y++)
                    for (int x = 0; x < boardSize; x++)
                        if (board[y][x] == '*' && bombTick[y][x] < bi) {
                            bi = bombTick[y][x];
                            by = y;
                            bx = x;
                        }
                if (bx >= 0) {
                    bombTick[by][bx] = 1;
                } else {
                    throw new RuntimeException("INFO: Trying to trigger a bomb, but no bombs planted.");
                }
            } else {
                throw new RuntimeException("INFO: Unknown move [" + mv + "]");
            }
            if (board[newY][newX] == '#' || board[newY][newX] == '+') {
                throw new RuntimeException("INFO: Bumping into wall.");
            } else if (board[newY][newX] == 'R') {
                Rc++;
                R += Rc;
                board[newY][newX] = '.';
            } else if (board[newY][newX] == 'B') {
                B++;
                Bc++;
                Bmax++;
                board[newY][newX] = '.';
            } else if (board[newY][newX] == 'T') {
                Tc++;
                T += Tc;
                board[newY][newX] = '.';
            }
            playY = newY;
            playX = newX;
        }
    }
}

class Drawer extends JFrame {
    public static final int EXTRA_WIDTH = 300;
    public static final int EXTRA_HEIGHT = 50;
    public TestCase tc;
    public DrawerPanel panel;
    public int cellSize, boardSize;
    public int width, height;

    class DrawerKeyListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            synchronized (keyMutex) {
                int keyCode = e.getKeyCode();
                switch (keyCode) {
                    case KeyEvent.VK_UP:
                        break;
                    case KeyEvent.VK_DOWN:
                        break;
                    case KeyEvent.VK_LEFT:
                        break;
                    case KeyEvent.VK_RIGHT:
                        break;
                }
                keyMutex.notifyAll();
            }
        }
    }

    class DrawerPanel extends JPanel {
        public void paint(Graphics g) {
            synchronized (tc.worldLock) {
                g.setFont(new Font("Arial", Font.BOLD, 12));

                g.setColor(new Color(32, 32, 32));
                g.fillRect(15, 15, cellSize * boardSize + 1, cellSize * boardSize + 1);
                g.setColor(Color.BLACK);
                for (int i = 0; i <= boardSize; i++) {
                    g.drawLine(15 + i * cellSize, 15, 15 + i * cellSize, 15 + cellSize * boardSize);
                    g.drawLine(15, 15 + i * cellSize, 15 + cellSize * boardSize, 15 + i * cellSize);
                }
                for (int i = 0; i < boardSize; i++) {
                    for (int j = 0; j < boardSize; j++) {
                        char ch = tc.board[i][j];
                        if (ch == '#') g.setColor(Color.BLUE);
                        else if (ch == '.') continue;
                        else if (ch == '+') g.setColor(new Color(139, 69, 19));
                        else if (ch == '?') g.setColor(Color.RED);
                        else if (ch == '@') g.setColor(Color.ORANGE);
                        else if (ch == 'R' || ch == 'B' || ch == 'T') {
                            g.setColor(Color.GREEN);
                            g.drawOval(15 + j * cellSize + 1, 15 + i * cellSize + 1, cellSize - 1, cellSize - 1);
                            g.setColor(Color.WHITE);
                            g.setFont(new Font("Arial", Font.BOLD, cellSize * 3 / 4));
                            g.drawString("" + ch, 15 + j * cellSize + cellSize * 2 / 8, 15 + (i + 1) * cellSize - cellSize * 1 / 8);
                            continue;
                        } else if (ch == '*') {
                            g.setColor(Color.RED);
                            g.drawOval(15 + j * cellSize + 1, 15 + i * cellSize + 1, cellSize - 1, cellSize - 1);
                            g.setColor(Color.WHITE);
                            g.setFont(new Font("Arial", Font.BOLD, cellSize / (tc.bombTick[i][j] > 9 ? 2 : 1)));
                            g.drawString("" + tc.bombTick[i][j], 15 + j * cellSize + 2, 15 + (i + 1) * cellSize);
                            continue;
                        }
                        g.fillRect(15 + j * cellSize + 1, 15 + i * cellSize + 1, cellSize - 1, cellSize - 1);
                    }
                }
                if (tc.board[tc.playY][tc.playX] == '?')
                    g.setColor(Color.BLACK);
                else
                    g.setColor(Color.RED);
                g.setFont(new Font("Arial", Font.BOLD, cellSize));
                g.drawString("X", 15 + tc.playX * cellSize + 2, 15 + (tc.playY + 1) * cellSize);

                g.setColor(Color.BLACK);
                g.setFont(new Font("Arial", Font.BOLD, 12));

                int horPos = 40 + boardSize * cellSize;
                Graphics2D g2 = (Graphics2D) g;
                g2.drawString("Board size = " + boardSize, horPos, 30);
                g2.drawString("Walls = " + tc.numWalls, horPos, 50);
                g2.drawString("Moves = " + tc.numMoves, horPos, 70);
                g2.drawString("Time to explode = " + tc.T + " (" + tc.Tc + " collected)", horPos, 90);
                g2.drawString("Bombs available = " + tc.B, horPos, 110);
                g2.drawString("Maximum bombs = " + tc.Bmax + " (" + tc.Bc + " collected)", horPos, 130);
                g2.drawString("Explosion range = " + tc.R + " (" + tc.Rc + " collected)", horPos, 150);
                g2.drawString("Score for move = " + tc.scoreLast, horPos, 170);
                g2.drawString("Score = " + tc.score, horPos, 190);
            }
        }
    }

    class DrawerWindowListener extends WindowAdapter {
        public void windowClosing(WindowEvent event) {
            System.exit(0);
        }
    }

    final Object keyMutex = new Object();

    public Drawer(TestCase tc_, int cellSize) {
        panel = new DrawerPanel();
        getContentPane().add(panel);
        addWindowListener(new DrawerWindowListener());
        this.tc = tc_;
        boardSize = tc.boardSize;
        this.cellSize = cellSize;
        width = cellSize * boardSize + EXTRA_WIDTH;
        height = cellSize * boardSize + EXTRA_HEIGHT;
        addKeyListener(new DrawerKeyListener());
        setSize(width, height);
        setTitle("XplosionMan Visualizer - TCO16 Marathon Match Finals");
        setResizable(false);
        setVisible(true);
    }
}


public class Tester {
    public static long seed = 1;
    public static boolean vis;
    public static int cellSize = 16;
    public static int delay = 100;

    public long runTest() throws Exception {
        TestCase tc = new TestCase(seed);
        Drawer drawer = null;
        if (vis) {
            drawer = new Drawer(tc, cellSize);
        }

        int maxMoves = tc.boardSize * tc.boardSize * 10;
        String[] boardStr = new String[tc.boardSize];
        for (int y = 0; y < tc.boardSize; y++) {
            boardStr[y] = String.valueOf(tc.board[y]);
//                boardStr[y] = "";
//                for (int x = 0; x < tc.boardSize; x++) {
//                    boardStr[y] += tc.board[y][x];
//                }
        }
        XplosionMan sol = new XplosionMan();
        String moves = sol.makeMoves(boardStr, tc.T, tc.B, tc.R);
        int numMoves = moves.length();
        if (numMoves > maxMoves) {
            System.err.println("ERROR: Return array from makeMoves too large.");
            return -1;
        }
        for (int i = 0; i < numMoves && tc.numMoves < maxMoves && tc.numWalls > 0; i++) {
            tc.numMoves++;
            tc.doMove(moves.charAt(i));
            if (vis) {
                drawer.repaint();
                try {
                    Thread.sleep(delay);
                } catch (Exception e) {
                    // do nothing
                }
            }
        }
        return tc.score;
    }

    public static void main(String[] args) throws Exception {
        for (int i = 0; i < args.length; i++)
            if (args[i].equals("-seed")) {
                seed = Long.parseLong(args[++i]);
            } else if (args[i].equals("-vis")) {
                vis = true;
            } else if (args[i].equals("-size")) {
                cellSize = Integer.parseInt(args[++i]);
            } else if (args[i].equals("-delay")) {
                delay = Integer.parseInt(args[++i]);
            } else {
                System.out.println("WARNING: unknown argument " + args[i] + ".");
            }

        Tester vis = new Tester();
        long score = vis.runTest();
        System.out.println("Score = " + score);
    }
}
